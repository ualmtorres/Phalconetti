<?php

$app = new \Phalcon\Mvc\Micro();

$app->get('/ejemplo', 'greeting');	// curl -i -X GET http://localhost/ProbandoPhalcon/api/ejemplo

// Sample function
function greeting() {

	// Prepare and send the data in JSON
	$response = array("Testing" => "PHP RESTful API for Redis using Phalcon");
	echo json_encode($response);
}

$app->handle();

?>